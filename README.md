# odroid-n2

Odroid N2/N2+ kernel with additional config flags that are needed for more advanced Docker functionality (swap, IPLAN, etc).  Natively complied on actual an actual N2. 


Kernel flags pulled from https://forum.odroid.com/viewtopic.php?f=61&t=38411.


https://github.com/moby/moby/blob/master/contrib/check-config.sh is used to ensure desired functionality.
